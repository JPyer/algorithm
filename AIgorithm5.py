# -*- coding: utf-8 -*-
"""
@Time ： 14/11/20 11:34 AM
@Auth ： Jier 
@Email：jwjier@gmail.com
"""

# 最快归并排序算法在Python中的纯实现
def merge_sort(collection):

	start,end = [],[]

	while len(collection) > 1:
		min_one,max_one = min(collection),max(collection)
		start.append(min_one)
		end.append(max_one)
		collection.remove(min_one)
		collection.remove(max_one)
	end.reverse()
	return start + collection + end

if __name__ == '__main__':
	user_input = input('Enter numbers separated by comma:\n').strip()
	unsorted = [int(item) for item in user_input.split(',')]
	print(*merge_sort(unsorted),sep=',')
