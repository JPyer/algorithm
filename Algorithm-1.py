# -*- coding: utf-8 -*-
"""
@Time ： 2020/11/11 14:38
@Auth ： Jier 
@Email：jwjier@gmail.com
"""
# O(n2)
def select_sort(data):
	'''
	:param data: An array wating to be sorted
	:return:
	'''
	for item in range(len(data) - 1):
		min_index = item
		for Times in range(item + 1,len(data)):
			if data[Times] < data[min_index]:
				min_index = Times
		data[item],data[min_index] = data[min_index],data[item]

if __name__ == '__main__':
	import random
	data_list = list(range(30))
	random.shuffle(data_list)
	print('pre:',data_list)
	select_sort(data_list)
	print('after:',data_list)